package cat.itb.cityquiz.presentation.screens.start;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.presentation.screens.quiz.CityQuizViewModel;

public class StartFragment extends Fragment {

    private CityQuizViewModel mViewModel;

    public static StartFragment newInstance() {
        return new StartFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.start_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.StartQuiz_button).setOnClickListener(this::changeToQuiz);

    }

    public void changeToQuiz(View view){
        mViewModel.startQuiz();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(CityQuizViewModel.class);
        mViewModel.getGameMutableLiveData().observe(this,this::onGameChanged);
        // TODO: Use the ViewModel
    }

    private void onGameChanged(Game game) {
        if (game!=null){
            Navigation.findNavController(getView()).navigate(R.id.action_startFragment_to_quizFragment);
        }
    }


}
