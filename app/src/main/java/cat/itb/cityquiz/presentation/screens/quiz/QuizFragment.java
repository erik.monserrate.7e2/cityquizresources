package cat.itb.cityquiz.presentation.screens.quiz;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.data.unsplashapi.imagedownloader.ImagesDownloader;
import cat.itb.cityquiz.domain.City;
import cat.itb.cityquiz.domain.Game;

public class QuizFragment extends Fragment {

    private CityQuizViewModel mViewModel;

    TextView opcio1;
    TextView opcio2;
    TextView opcio3;
    TextView opcio4;
    TextView opcio5;
    TextView opcio6;


    public static QuizFragment newInstance() {
        return new QuizFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.quiz_fragment_row, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(CityQuizViewModel.class);
        mViewModel.getGameMutableLiveData().observe(this,this::onGameChanged);
    }

    private void onGameChanged(Game game){
        if (!game.isFinished()){
            display(game);
        }else {
            changeToResultFragment(game);
        }
    }

    private void display(Game game) {
        String fileName = ImagesDownloader.scapeName(game.getCurrentQuestion().getCorrectCity().getName());
        game.getCurrentQuestion().getCorrectCity();
        int resId = getContext().getResources().getIdentifier(fileName, "drawable", getContext().getPackageName());
        ImageView imageView = getView().findViewById(R.id.cityImage);
        imageView.setImageResource(resId);

        opcio1 = getView().findViewById(R.id.btn_1);
        opcio2 = getView().findViewById(R.id.btn_2);
        opcio3 = getView().findViewById(R.id.BTN_3);
        opcio4 = getView().findViewById(R.id.BTN_4);
        opcio5 = getView().findViewById(R.id.BTN_5);
        opcio6 = getView().findViewById(R.id.BTN_6);

        List<City> answers = game.getCurrentQuestion().getPossibleCities();

        opcio1.setText(answers.get(0).getName());
        opcio2.setText(answers.get(1).getName());
        opcio3.setText(answers.get(2).getName());
        opcio4.setText(answers.get(3).getName());
        opcio5.setText(answers.get(4).getName());
        opcio6.setText(answers.get(5).getName());

        opcio1.setOnClickListener(this::onViewClicked);
        opcio2.setOnClickListener(this::onViewClicked);
        opcio3.setOnClickListener(this::onViewClicked);
        opcio4.setOnClickListener(this::onViewClicked);
        opcio5.setOnClickListener(this::onViewClicked);
        opcio6.setOnClickListener(this::onViewClicked);

    }

    public void questionAnswered(int i){
        mViewModel.answerQuestions(i);

    }

    public int onViewClicked(View view){
        int response = 0;
        switch (view.getId()){
            case R.id.btn_1:
                response = 0;
                questionAnswered(response);
                break;
            case R.id.btn_2:
                response = 1;
                questionAnswered(response);
                break;
            case R.id.BTN_3:
                response = 2;
                questionAnswered(response);
                break;
            case R.id.BTN_4:
                response = 3;
                questionAnswered(response);
                break;
            case R.id.BTN_5:
                response = 4;
                questionAnswered(response);
                break;
            case R.id.BTN_6:
                response = 5;
                questionAnswered(response);
                break;
        }
        return response;
    }

    public void changeToResultFragment(Game game){
        Navigation.findNavController(getView()).navigate(R.id.action_quizFragment_to_resultFragment);
    }

}
