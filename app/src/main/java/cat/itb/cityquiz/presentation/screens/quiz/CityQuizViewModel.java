package cat.itb.cityquiz.presentation.screens.quiz;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import cat.itb.cityquiz.domain.City;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.repository.GameLogic;
import cat.itb.cityquiz.repository.RepositoriesFactory;

public class CityQuizViewModel extends ViewModel {
    GameLogic gameLogic = RepositoriesFactory.getGameLogic();
    MutableLiveData<Game> gameMutableLiveData = new MutableLiveData<>();


    public void startQuiz() {
        Game game = gameLogic.createGame(Game.maxQuestions,Game.possibleAnswers);
        gameMutableLiveData.postValue(game);
    }


    public void answerQuestions(int response) {
        Game game = gameLogic.answerQuestions(gameMutableLiveData.getValue(),response);
        gameMutableLiveData.postValue(game);
    }

    public GameLogic getGameLogic() {
        return gameLogic;
    }

    public void setGameLogic(GameLogic gameLogic) {
        this.gameLogic = gameLogic;
    }

    public MutableLiveData<Game> getGameMutableLiveData() {
        return gameMutableLiveData;
    }
}
