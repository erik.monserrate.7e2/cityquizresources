package cat.itb.cityquiz.presentation.screens.result;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.presentation.screens.quiz.CityQuizViewModel;

public class ResultFragment extends Fragment {

    private CityQuizViewModel mViewModel;
    private Game game;
    private TextView textView;
    private int score;

    public static ResultFragment newInstance() {
        return new ResultFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.result_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(CityQuizViewModel.class);
        mViewModel.getGameMutableLiveData().observe(this,this::onGameChanged);
        getView().findViewById(R.id.playAgainBTN).setOnClickListener(this::changeToStartFragment);
    }

    private void onGameChanged(Game game) {
        if (game.isFinished()){
            score =game.getScore();
            textView = getView().findViewById(R.id.ScoreNumberView);
            textView.setText(game.getNumCorrectAnswers()+"");
        }else {
            Navigation.findNavController(getView()).navigate(R.id.action_resultFragment_to_startFragment);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        view.findViewById(R.id.playAgainBTN).setOnClickListener(this::changeToStartFragment);
    }

    public void changeToStartFragment(View view){
        mViewModel.startQuiz();

    }
}
