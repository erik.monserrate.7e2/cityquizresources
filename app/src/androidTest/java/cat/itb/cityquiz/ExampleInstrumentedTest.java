package cat.itb.cityquiz;

import android.content.Context;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import cat.itb.cityquiz.presentation.MainActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isCompletelyDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void playTest() {
        //comprovem que es mostra el botó de començar i clckem
        onView(withId(R.id.StartQuiz_button))
                .check(matches(isDisplayed()))
                .perform(click());

        onView(withId(R.id.quiz_fragment_row))//comprovar que es mostren tots els elements del quiz, imatge i butons
                .check(matches(isCompletelyDisplayed()));



        //Pulsem un boto diferent cada cop cinc cops, un per cada pregunta
        onView(withId(R.id.BTN_6)).perform(click());
        onView(withId(R.id.btn_1)).perform(click());
        onView(withId(R.id.BTN_4)).perform(click());
        onView(withId(R.id.BTN_3)).perform(click());
        onView(withId(R.id.btn_2)).perform(click());

        //Pantalla final comprovem que es mostra la puntuació
        onView(withId(R.id.ScoreNumberView)).check(matches(isDisplayed()));

        onView(withId(R.id.playAgainBTN)).check(matches(isDisplayed()))
                .perform(click());//i clickem


    }
}
